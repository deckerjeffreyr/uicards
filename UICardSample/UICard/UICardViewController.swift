//
//  UICardViewController.swift
//  UICardSample
//
//  Created by Jeffrey Decker on 10/26/16.
//  Copyright © 2016 itsdecker. All rights reserved.
//

import UIKit

// TODO: To make this usable, we need to create a card view data provider similar to what collection views use
// to initialize each card. This includes creating a base UICardView class and asking for cards via index path.

class UICardViewController: UIViewController {
    var allowVerticalScroll : Bool = true
    var enableCircularVerticalScroll : Bool = true
    var allowHorizontalScroll : Bool = true
    var enableHorizontalVerticalScroll : Bool = true
    var supportedSwipeDirections : [UISwipeGestureRecognizerDirection] = [.up, .down, .left, .right]
    
    var cardPadding : CGFloat = 40.0
    var cardHint : CGFloat = 0.0 // TODO: start using me to space out the cards and determine card size
    // TODO: The work to support hints or previews isn't trivial and requires some major refactoring. Any translation
    // we do requires that prepare an extra card at the end of all 3 visible rows or columns in the direction in which 
    // we are exposing a new card in order to have a preview of the next card there when the animation ends. This should
    // be done before animating and means that instead of recycling the last view in the swipe direction, we will
    // discard it after the animation is done and create a new view before the animation to replace it. Some of this work is
    // shared between pan and swipe and some will need to be done in each of those implementations. Another thing to consider
    // is that in preview cases, all cards in view need to move, so there are actually be 9 cards visible. This will likely
    // require a 2d array of cards to support preview.
    
    var verticalCards : [UIView] = []
    var horizontalCards : [UIView] = []
    
    var activeCard : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addCards()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        // TODO: destroy all cards
    }

    func cardHeight() -> CGFloat {
        return view.frame.height - (2 * cardPadding) // - (2 * hintSize)
    }
    
    func cardWidth() -> CGFloat {
        return view.frame.width - (2 * cardPadding) // - (2 * hintSize)
    }
    
    func cardSpaceX() -> CGFloat {
        return view.bounds.width - cardPadding
    }
    
    func cardSpaceY() -> CGFloat {
        return view.bounds.height - cardPadding
    }
    
    func addCards() {
        activeCard = UIView(frame:
            CGRect(x: view.bounds.origin.x + cardPadding,
                   y: view.bounds.origin.y + cardPadding,
                   width: cardWidth(),
                   height: cardHeight()))
        activeCard.backgroundColor = UIColor.blue
        view.addSubview(activeCard)
        
        addVerticalCards()
        addHorizontalCards()
    }
    
    func addVerticalCards() {
        if allowVerticalScroll {
            // Add a top, middle (active) and bottom cards to the view array
            let topCard = UIView(frame:
                CGRect(x: view.bounds.origin.x + cardPadding,
                       y: -cardHeight(),
                       width: cardWidth(),
                       height: cardHeight()))
            let bottomCard = UIView(frame:
                CGRect(x: view.bounds.origin.x + cardPadding,
                       y: view.bounds.height,
                       width: cardWidth(),
                       height: cardHeight()))
            
            topCard.backgroundColor = UIColor.red
            bottomCard.backgroundColor = UIColor.yellow
            
            view.addSubview(topCard)
            view.addSubview(bottomCard)
            
            verticalCards.append(topCard)
            verticalCards.append(activeCard)
            verticalCards.append(bottomCard)
        } else {
            verticalCards.append(activeCard)
        }
    }
    
    func addHorizontalCards() {
        if allowHorizontalScroll {
            // Add a left, middle (active) and right cards to the view array
            let leftCard = UIView(frame:
                CGRect(x: -cardWidth(),
                       y: view.bounds.origin.y + cardPadding,
                       width: cardWidth(),
                       height: cardHeight()))
            let rightCard = UIView(frame:
                CGRect(x: view.bounds.width,
                       y: view.bounds.origin.y + cardPadding,
                       width: cardWidth(),
                       height: cardHeight()))
            
            leftCard.backgroundColor = UIColor.green
            rightCard.backgroundColor = UIColor.purple
            
            view.addSubview(leftCard)
            view.addSubview(rightCard)
            
            horizontalCards.append(leftCard)
            horizontalCards.append(activeCard)
            horizontalCards.append(rightCard)
            
        } else {
            horizontalCards.append(activeCard)
        }
    }
    
    func updateActiveCardAfterVerticalScroll(_ direction: UISwipeGestureRecognizerDirection) {
        switch direction {
        case UISwipeGestureRecognizerDirection.up:
            let firstView = self.verticalCards.removeFirst()
            var viewFrame = firstView.frame
            viewFrame.origin.y += ((viewFrame.height + cardPadding) * 3)
            firstView.frame = viewFrame
            self.verticalCards.append(firstView)
        case UISwipeGestureRecognizerDirection.down:
            let lastView = self.verticalCards.removeLast()
            var viewFrame = lastView.frame
            viewFrame.origin.y -= ((viewFrame.height + cardPadding) * 3)
            lastView.frame = viewFrame
            self.verticalCards.insert(lastView, at: 0)
        default:
            break
        }
        
        activeCard = verticalCards[1]
        
        if allowHorizontalScroll {
            horizontalCards.remove(at: 1)
            horizontalCards.insert(activeCard, at: 1)
        } else {
            horizontalCards.append(activeCard)
            horizontalCards.removeFirst()
        }
    }
    
    func updateActiveCardAfterHorizontalScroll(_ direction: UISwipeGestureRecognizerDirection) {
        switch direction {
        case UISwipeGestureRecognizerDirection.left:
            let firstView = self.horizontalCards.removeFirst()
            var viewFrame = firstView.frame
            viewFrame.origin.x += ((viewFrame.width + cardPadding) * 3)
            firstView.frame = viewFrame
            self.horizontalCards.append(firstView)
        case UISwipeGestureRecognizerDirection.right:
            let lastView = self.horizontalCards.removeLast()
            var viewFrame = lastView.frame
            viewFrame.origin.x -= ((viewFrame.width + cardPadding) * 3)
            lastView.frame = viewFrame
            self.horizontalCards.insert(lastView, at: 0)
        default:
            break
        }
        
        activeCard = horizontalCards[1]
        
        if allowVerticalScroll {
            verticalCards.remove(at: 1)
            verticalCards.insert(activeCard, at: 1)
        } else {
            verticalCards.append(activeCard)
            verticalCards.removeFirst()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
