//
//  UISwipeCardViewController.swift
//  UICardSample
//
//  Created by Jeffrey Decker on 10/28/16.
//  Copyright © 2016 itsdecker. All rights reserved.
//

import UIKit

class UISwipeCardViewController: UICardViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSwipeGestures()
    }
    
    func addSwipeGestures() {
        for direction in supportedSwipeDirections {
            let gesture = UISwipeGestureRecognizer(target: self, action: #selector(UISwipeCardViewController.handleSwipeGesture(_:)))
            gesture.direction = direction
            self.view.addGestureRecognizer(gesture)
        }
    }
    
    // MARK: - Swipe Gesture Handling
    
    func handleSwipeGesture(_ sender: UISwipeGestureRecognizer) {
        print(sender.direction)
        
        switch sender.direction {
        case UISwipeGestureRecognizerDirection.up:
            doSwipeUp()
        case UISwipeGestureRecognizerDirection.down:
            doSwipeDown()
        case UISwipeGestureRecognizerDirection.left:
            doSwipeLeft()
        case UISwipeGestureRecognizerDirection.right:
            doSwipeRight()
        default:
            print("Unrecognized swipe direction")
        }
    }
    
    func doSwipeUp() {
        if allowVerticalScroll {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                for view : UIView in self.verticalCards {
                    var viewFrame = view.frame
                    viewFrame.origin.y -= self.cardHeight() + self.cardPadding
                    view.frame = viewFrame
                }
                }, completion: { finished in
                    print("Translated Up")
                    self.updateActiveCardAfterVerticalScroll(.up)
            })
        }
    }
    
    func doSwipeDown() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            for view : UIView in self.verticalCards {
                var viewFrame = view.frame
                viewFrame.origin.y += self.cardHeight() + self.cardPadding
                view.frame = viewFrame
            }
            }, completion: { finished in
                print("Translated Down")
                self.updateActiveCardAfterVerticalScroll(.down)
        })
    }
    
    func doSwipeLeft() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            for view : UIView in self.horizontalCards {
                var viewFrame = view.frame
                viewFrame.origin.x -= view.frame.width + self.cardPadding
                view.frame = viewFrame
            }
            }, completion: { finished in
                print("Translated Left")
                self.updateActiveCardAfterHorizontalScroll(.left)
        })
        
    }
    
    func doSwipeRight() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
            for view : UIView in self.horizontalCards {
                var viewFrame = view.frame
                viewFrame.origin.x += view.frame.width + self.cardPadding
                view.frame = viewFrame
            }
            }, completion: { finished in
                print("Translated Right")
                self.updateActiveCardAfterHorizontalScroll(.right)
        })
    }
}
