//
//  UIPanCardViewController.swift
//  UICardSample
//
//  Created by Jeffrey Decker on 10/28/16.
//  Copyright © 2016 itsdecker. All rights reserved.
//

import UIKit

class UIPanCardViewController: UICardViewController {
    enum PanDirection {
        case none, up, down, left, right
    }
    
    private final var VELOCITY_THRESHOLD : CGFloat = 500.0
    private final var DIRECTION_THRESHOLD : CGFloat = 1.333
    
    var panGestureDirection : PanDirection = .none
    var lastPanTranslation : CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addSwipeGestures()
    }
    
    func addSwipeGestures() {
        self.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(UIPanCardViewController.handlePanGesture(_:))))
    }
    
    // MARK: - Pan Gesture Handling
    func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            handlePanBegan(sender)
        case .changed:
            handlePanChanged(sender)
        case .ended:
            handlePanEnded(sender)
        default:
            break
        }
    }
    
    // TODO: Start keeping track of when a direction is actually detected. In some cases the user can swipe in a direction
    // but their initial reading doesn't indicate that direction. In these cases we should wait to see if we can ever decipher
    // a direction and start acting on that direction after it's detected provided the translation distance isn't too far already.
    func handlePanBegan(_ sender: UIPanGestureRecognizer) {
        let velocity :CGPoint = sender.velocity(in: view)
        
        let xRatio = velocity.x / velocity.y
        let yRatio = velocity.y / velocity.x
        
        if yRatio <= -DIRECTION_THRESHOLD {
            panGestureDirection = .up
        } else if yRatio >= DIRECTION_THRESHOLD {
            panGestureDirection = .down
        } else if xRatio <= -DIRECTION_THRESHOLD {
            panGestureDirection = .left
        } else if xRatio >= DIRECTION_THRESHOLD {
            panGestureDirection = .right
        } else {
            panGestureDirection = .none
        }
        
        updateCardPositions(sender: sender)
    }
    
    func handlePanChanged(_ sender: UIPanGestureRecognizer) {
        updateCardPositions(sender: sender)
    }
    
    func handlePanEnded(_ sender: UIPanGestureRecognizer) {
        updateCardPositions(sender: sender)
        snapToPosition(sender: sender)
        
        // Reset Pan
        panGestureDirection = .none
        lastPanTranslation = CGPoint(x: 0.0, y: 0.0)
    }
    
    func updateCardPositions(sender: UIPanGestureRecognizer) {
        if panGestureDirection == .none {
            return
        }
        // Get translation position and delta from last translation
        let translation = sender.translation(in: self.view)
        let deltaX = translation.x - lastPanTranslation.x
        let deltaY = translation.y - lastPanTranslation.y
        
        // Update card positions based on translation delta
        if panGestureDirection == .up || panGestureDirection == .down {
            for view : UIView in self.verticalCards {
                var viewFrame = view.frame
                viewFrame.origin.y += deltaY
                view.frame = viewFrame
            }
        } else if panGestureDirection == .left || panGestureDirection == .right {
            for view : UIView in self.horizontalCards {
                var viewFrame = view.frame
                viewFrame.origin.x += deltaX
                view.frame = viewFrame
            }
        }
        // Set point for
        lastPanTranslation = translation
    }

    func snapToPosition(sender: UIPanGestureRecognizer) {
        if panGestureDirection == .none {
            return
        }
        
        // Update card positions
        if panGestureDirection == .up || panGestureDirection == .down {
            verticalSnap(sender: sender)
        } else if panGestureDirection == .left || panGestureDirection == .right {
            horizontalSnap(sender: sender)
        }
    }
    
    // TODO: Still some things that don't feel right, mainly, if you start panning in one direction then change course with
    // a hard pan, you can end up on the card in the opposide direction. Logically it makes sense but it might be a wierd thing
    // for users. This is pretty minor so for now I think it's alright, but something to consider for future work.
    
    // TODO: it feels like some of the snap logic for vertical and horizontal overlaps, maybe there's some thing we can do
    // to use CGPoint for adjustment? After everything this working with velocity taken into account, lets revisit
    
    func verticalSnap(sender: UIPanGestureRecognizer) {
        let translation : CGPoint = sender.translation(in: view)
        let velocity : CGPoint = sender.velocity(in: view)
        var adjustment : CGFloat = 0.0
        var direction : UISwipeGestureRecognizerDirection = .up
        var fullMove : Bool = false
        
        // If one of the thresholds are met to advance (translation or velocity) and both agree in direction
        // then advance all cards in that direction. Otherwise, reset the cards back to their original position
        if (abs(translation.y) > (cardSpaceY() / 2) || abs(velocity.y) >= VELOCITY_THRESHOLD)
            && ((translation.y > 0 && velocity.y > 0) || (translation.y < 0 && velocity.y < 0)){
            fullMove = true
            if translation.y > 0.0 {
                direction = .down
                adjustment = cardSpaceY() - translation.y
            } else if translation.y < 0.0 {
                adjustment = -(cardSpaceY() + translation.y)
            }
        } else {
            if translation.x > 0.0 {
                direction = .down
            }
            adjustment = -(translation.y) // Move the cards back to where they started
        }
        
        var duration : TimeInterval = TimeInterval(abs(adjustment)) / TimeInterval(VELOCITY_THRESHOLD)
        if abs(velocity.y) >= VELOCITY_THRESHOLD {
            duration = TimeInterval(abs(adjustment)) / TimeInterval(abs(velocity.y))
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            for view : UIView in self.verticalCards {
                var viewFrame = view.frame
                viewFrame.origin.y += adjustment
                view.frame = viewFrame
            }
        }, completion: { finished in
            if fullMove {
                self.updateActiveCardAfterVerticalScroll(direction)
            }
        });
    }
    
    func horizontalSnap(sender: UIPanGestureRecognizer) {
        let translation : CGPoint = sender.translation(in: view)
        let velocity : CGPoint = sender.velocity(in: view)
        var adjustment : CGFloat = 0.0
        var direction : UISwipeGestureRecognizerDirection = .left
        var fullMove : Bool = false
        
        // If one of the thresholds are met to advance (translation or velocity) and both agree in direction
        // then advance all cards in that direction. Otherwise, reset the cards back to their original position
        if (abs(translation.x) > (cardSpaceX() / 2)  || abs(velocity.x) >= VELOCITY_THRESHOLD)
            && ((translation.x > 0 && velocity.x > 0) || (translation.x < 0 && velocity.x < 0)) {
            fullMove = true
            if translation.x > 0.0 {
                direction = .right
                adjustment = cardSpaceX() - translation.x
            } else if translation.x < 0.0 {
                adjustment = -(cardSpaceX() + translation.x)
            }
        } else {
            if translation.x > 0.0 {
                direction = .right
            }
            adjustment = -(translation.x) // Move the cards back to where they started
        }
        
        var duration : TimeInterval = TimeInterval(abs(adjustment)) / TimeInterval(VELOCITY_THRESHOLD)
        if abs(velocity.x) >= VELOCITY_THRESHOLD {
            duration = TimeInterval(abs(adjustment)) / TimeInterval(abs(velocity.x))
        }
        
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseOut, animations: {
            for view : UIView in self.horizontalCards {
                var viewFrame = view.frame
                viewFrame.origin.x += adjustment
                view.frame = viewFrame
            }
        }, completion: { finished in
            if fullMove {
                self.updateActiveCardAfterHorizontalScroll(direction)
            }
        })
    }

}
